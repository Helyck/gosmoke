package com.helyck.gosmoke;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import org.apache.http.client.methods.HttpGet;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;


public class DeskFragment extends Fragment implements View.OnClickListener {

    Button addButton;

    private OnFragmentInteractionListener mListener;

    public static DeskFragment newInstance(String param1, String param2) {
        DeskFragment fragment = new DeskFragment();

        return fragment;
    }

    public DeskFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_desk, container, false);

        loadContacts(view);

        addButton = (Button) view.findViewById(R.id.desk_add_button);
        addButton.setOnClickListener(this);

        return view;
    }

    private void loadContacts(View view) {
        MainActivity activity = (MainActivity) getActivity();
        ArrayList<String> contacts = activity.getContacts();

        LinearLayout layout = (LinearLayout) view.findViewById(R.id.desk_mainLayout);
        for(int i = 0; i < contacts.size(); ++i){
            String phone = contacts.get(i);
            PingButton button = new PingButton(getActivity(), phone);
            button.setOnClickListener(this);
            layout.addView(button);
        }

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        try {
            PingButton pingButton = (PingButton) v;

            String phone = pingButton.getPhone();
            ping(phone);


        }catch (ClassCastException e) {
            switch (v.getId()) {
                case R.id.desk_add_button:
                    MainActivity activity = (MainActivity) getActivity();
                    activity.toFind();
                    break;
            }
        }
    }

    public interface OnFragmentInteractionListener {

        public void onFragmentInteraction(Uri uri);
    }

    private void ping(String phone){
        Log.d("devTime", "pinged phone " + phone);
        //TODO 04 implement

        MainActivity activity = (MainActivity) getActivity();
        String host = activity.getHost();
        String path = host + "/api/ping/";
        String code = activity.getCode();
        String fullPath = path + code;
        Log.d("devTime", "GET: " + fullPath);
        HttpGet httpGet = null;
        try {
            httpGet = new HttpGet(new URI(fullPath));
            PingTask task = new PingTask(activity);
            task.execute(httpGet);
        } catch (URISyntaxException e) {
            //TODO 05 message about connection fail
            e.printStackTrace();
        }
    }
}
