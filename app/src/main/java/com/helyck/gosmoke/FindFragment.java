package com.helyck.gosmoke;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.apache.http.client.methods.HttpGet;

import java.net.URI;
import java.net.URISyntaxException;


public class FindFragment extends Fragment implements View.OnClickListener {
    private EditText searchField;
    private Button addButton;

    private OnFragmentInteractionListener mListener;

    public static FindFragment newInstance(String param1, String param2) {
        FindFragment fragment = new FindFragment();

        return fragment;
    }

    public FindFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_find, container, false);

        searchField = (EditText) view.findViewById(R.id.find_phone_field);
        addButton = (Button) view.findViewById(R.id.find_add_button);
        addButton.setOnClickListener(this);

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.find_add_button:
                onClickAdd();
                break;
        }
    }

    private void onClickAdd() {
        MainActivity activity = (MainActivity) getActivity();
        Log.d("guiEvent", "FindFragment click");
        //TODO 05 normal implementation
        String phone = searchField.getText().toString();
        if (verifyPhoneNumber(phone)){
            String host = activity.getHost();
            String path = host + "/api/addContact/";
            String code = activity.getCode();
            String fullPath = path + code + "/" + phone;
            Log.d("devTime", "GET: " + fullPath);
            HttpGet httpGet = null;
            try {
                httpGet = new HttpGet(new URI(fullPath));
                AddContactTask task = new AddContactTask(activity, phone);
                task.execute(httpGet);
            } catch (URISyntaxException e) {
                //TODO 05 message about connection fail
                e.printStackTrace();
            }

        }else{
            activity.toDesk();
        }

    }

    private boolean verifyPhoneNumber(String string){
        if (string.equals("")){
            return false;
        }else{
            //TODO 05 implement
            return true;
        }
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }

}
