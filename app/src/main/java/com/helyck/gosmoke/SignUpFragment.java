package com.helyck.gosmoke;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import java.net.URI;

public class SignUpFragment extends Fragment implements View.OnClickListener {

    EditText phoneEditText;
    Button button;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SignUpFragment.
     */

    public static SignUpFragment newInstance(String param1, String param2) {
        SignUpFragment fragment = new SignUpFragment();

        return fragment;
    }

    public SignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);

        phoneEditText = (EditText) view.findViewById(R.id.signUp_phone_editText);
        button = (Button) view.findViewById(R.id.signUp_confirm_button);
        button.setOnClickListener(this);

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        Log.d("guiEvent", "signUpFragment: button click");
        //SignUpTask task = new SignUpTask();
        //Log.d("guiEvent", "signUpFragment: task created");
        //task.execute("10.14.0.112:5000");

        String phoneText = phoneEditText.getText().toString();

        if (verifyPhoneSyntax(phoneText)){
            // the request
            try
            {
                MainActivity activity = (MainActivity) getActivity();
                String host = activity.getHost();

                String path = host + "/api/verifyPhone/";

                String fullPath = path + phoneText;
                Log.d("devTime", "Post to: " + fullPath);
                HttpGet httpGet = new HttpGet(new URI(fullPath));

                SignUpTask task = new SignUpTask(activity);
                task.execute(httpGet);

            }
            catch (Exception e)
            {
                //TODO 05 message about connection fail
                Log.e("devTime", e.getMessage());
            }
        }else{
            //TODO 05 message about correct phone
        }


    }

    private boolean verifyPhoneSyntax(String string){
        //TODO 05 implement
        return true;
    }

    public interface OnFragmentInteractionListener {

        public void onFragmentInteraction(Uri uri);
    }

}
