package com.helyck.gosmoke;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class PingTask extends AsyncTask<HttpUriRequest, Void, String> {
    MainActivity activity;

    public PingTask(MainActivity activity){
        this.activity = activity;
    }

    @Override
    protected String doInBackground(HttpUriRequest... params)
    {
        HttpClient mClient = new DefaultHttpClient();
        try
        {
            HttpUriRequest request = params[0];
            HttpResponse serverResponse = mClient.execute(request);
            BasicResponseHandler handler = new BasicResponseHandler();
            return handler.handleResponse(serverResponse);
        }
        catch (Exception e)
        {

            e.printStackTrace();
            return "";
        }
    }

    // convert InputStream to String
    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        Log.d("devTime", "Ping result: " + result);
        if (result.equals("")){

        }else{
            JSONObject dataJsonObj = null;

            /*
            try {
                //dataJsonObj = new JSONObject(result);
                //Boolean addedStatus = dataJsonObj.getBoolean("added");

                //if (addedStatus){
                //    activity.onAddContact(phone);
                //}

                //activity.toDesk();

            } catch (JSONException e) {
                e.printStackTrace();

                //TODO 05 check processing
                //activity.toDesk();
                //activity.reportSignUpFail();
            }
*/
        }

    }
}