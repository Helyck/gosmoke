package com.helyck.gosmoke;

import android.content.Context;
import android.widget.Button;


public class PingButton extends Button {
    private String phone;

    public PingButton(Context context, String phone) {
        super(context);

        this.phone = phone;

        this.setText(phone);
    }

    public String getPhone(){
        return phone;
    }
}
