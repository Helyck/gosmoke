package com.helyck.gosmoke;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;


public class MainActivity extends FragmentActivity implements
        SignUpFragment.OnFragmentInteractionListener,
        VerifyCodeFragment.OnFragmentInteractionListener,
        DeskFragment.OnFragmentInteractionListener,
        FindFragment.OnFragmentInteractionListener{

    private String host = "http://10.14.0.112:5000";
    private String code = "";
    private ArrayList<String> contacts = new ArrayList<>();//govnocode

    public String getHost(){
        return host;
    }

    public void setCode(String code){
        Log.d("devTime", "code: " + code);

        this.code = code;

        SharedPreferences preferences = this.getPreferences(this.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("code", code);
        editor.commit();


    }

    public String getCode(){
        return code;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadContacts();

        SharedPreferences preferences = this.getPreferences(this.MODE_PRIVATE);
        code = preferences.getString("code", "");
        Log.d("devTime", "code at start: " + code);



        if (code.equals("")){
            toSignUp();
        }else{
            toDesk();
        }
    }

    public void toSignUp() {
        toFragment(new SignUpFragment());
    }

    public void toDesk(){
        toFragment(new DeskFragment());
    }

    public void toFind(){
        toFragment(new FindFragment());
    }

    private void toFragment(Fragment fragment){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, fragment);
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void onAddContact(String phone){
        if (!contacts.contains(phone)){
            contacts.add(phone);

            SharedPreferences preferences = this.getPreferences(Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("saved_contacts", contactsToString());
            editor.commit();
        }
    }

    private void loadContacts(){
        SharedPreferences preferences = this.getPreferences(Activity.MODE_PRIVATE);
        String contactsData = preferences.getString("saved_contacts", "");

        Log.d("devTime", "saved contacts data: " + contactsData);

        if (!contactsData.equals("")){
            String [] splited = contactsData.split("\\.");
            Log.d("devTime", "splited contacts count " + splited.length);
            for(int i = 0; i < splited.length; ++i){
                String phone = splited[i];
                if (!contacts.contains(phone)) {
                    contacts.add(phone);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        //TODO 04 is need implement?
    }

    public void reportSignUpFail() {
        //TODO 04 implement
    }

    private String contactsToString(){
        StringBuilder result = new StringBuilder("");

        if (contacts.size() > 0){
            result.append(contacts.get(0));

            for(int i = 1; i < contacts.size(); ++i){
                result.append(".");
                result.append(contacts.get(i));
            }
        }

        return result.toString();
    }

    public ArrayList<String> getContacts() {
        return contacts;
    }
}
